import { CronJob } from 'cron'
import { checkBalanceValues } from './checkBalanceSystem'
import { databaseBackup } from './databaseBackup'
import { fixBalances } from './fixBalancesCron'
import { NODE_ENV } from '../config/env'

if (NODE_ENV == 'staging') {
  new CronJob('0 0 */12 * * *', function () {
    console.log('--------------> Backup System Cron just start !', new Date())
    return databaseBackup()
  }, null, true)

  new CronJob('0 5 */12 * * *', function () {
    console.log('--------------> Check Balance Cron just start !', new Date())
    return checkBalanceValues()
  }, null, true)
}

if (NODE_ENV == 'production') {
  new CronJob('0 */15 * * * *', function () {
    console.log('--------------> Fix Balances Cron just start !', new Date())
    return fixBalances()
  }, null, true)
}
