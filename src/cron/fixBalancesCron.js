import fs from 'fs'
import path from 'path'
import { Driver, Transaction } from '../config/models'

export async function fixBalances() {
  try {

    let ch
    let total = 0
    const logPath = path.join(__dirname, '/../../../logs/checkBalance.txt')


    const drivers = await Driver.find()

    for (let driver of drivers) {
      const transactions = await Transaction.find({ driverModelId: driver._id })
      let sum = transactions.reduce((s, item) => s + item.balance, 0)

      sum = sum.toFixed(2)
      driver.balance = driver.balance.toFixed(2)

      if (sum != driver.balance) {

        if (total == 0) {
          ch = '\n\n                      ' + new Date().toISOString() + '\n'
          await fs.appendFileSync(logPath, ch)
        }

        total++

        ch = '\n-------------------------\n' +
          'Balance check error for driver ' + driver.captainId +
          ':\nDriver registred balance ' + driver.balance +
          '\nCalculated balance ' + sum +
          '\nDifference ' + (driver.balance - sum).toFixed(2)

        await fs.appendFileSync(logPath, ch)

        driver.balance = parseFloat(sum)
        await driver.save()
      }
    }

    if (total != 0) {
      ch = '\n-------------------------\n' +
        'Total errors found ' + total

      await fs.appendFileSync(logPath, ch)
    }

    console.log('--------------> Fix Balance Cron finished with success !', new Date())
    return

  } catch (error) {
    console.log('--------------> Fix Balance Cron had an error', new Date())
    console.log(error)
  }
}
