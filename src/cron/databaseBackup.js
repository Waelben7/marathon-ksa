import shell from 'shelljs'
import { DHOST, DDB, DUSER, DPASS, RHOST, RDB, RUSER, RPASS } from '../config/env';

export async function databaseBackup() {
  try {

    shell.cd('../dump')
    let dumps = shell.exec('ls -tr').stdout.split('\n')
    dumps.pop()

    if (dumps.length > 60)
      shell.rm('-rf', dumps[0])

    const dateString = new Date().toISOString().substr(0, 16)

    shell.exec('mongodump --host ' + DHOST + ' --db ' + DDB + ' --username ' + DUSER
      + ' --password ' + DPASS + ' --out ./' + dateString)

    shell.exec('mongorestore --host ' + RHOST + ' --drop --db ' + RDB + ' --username ' + RUSER
      + ' --password ' + RPASS + ' ./' + dateString + '/marathon')  //  + RDB

    console.log('--------------> Backup Cron finished with success !', new Date())
    return

  } catch (error) {
    console.log('--------------> Backup Cron had an error', new Date())
    console.log(error)
  }
}

export async function backupFromRoute(req, res) {
  await databaseBackup()
  return res.json({ success: true })
}