import express from 'express'
const router = express.Router()

import staticRoute from '../components/static/staticRoute'
import authRoute from '../components/authorization/authRoute'
import uploadRoute from '../components/upload/uploadRoute'
import userRoute from '../components/user/userRoute'
import driverRoute from '../components/driver/driverRoute'
import transactionRoute from '../components/transaction/transactionRoute'
import carriagePaymentRoute from '../components/carriagePayment/carriagePaymentRoute'
import reportRoute from '../components/report/reportRoute'
import receivedCashtRoute from '../components/receivedCash/receivedCashRoute'
import CamOrderRoute from '../components/camOrder/camOrderRoute'
import SysAdminRoute from '../components/sysAdmin/sysAdminRoute'

staticRoute(router)
authRoute(router)
uploadRoute(router)
userRoute(router)
driverRoute(router)
transactionRoute(router)
carriagePaymentRoute(router)
reportRoute(router)
receivedCashtRoute(router)
CamOrderRoute(router)
SysAdminRoute(router)

export default router
