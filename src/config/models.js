import User from '../components/user/user'
import Driver from '../components/driver/driver'
import Transaction from '../components/transaction/transaction'
import CarriagePayment from '../components/carriagePayment/carriagePayment'
import Report from '../components/report/report'
import ReceivedCash from '../components/receivedCash/receivedCash'
import CamOrder from '../components/camOrder/camOrder'

export {
  User, Driver, Transaction, CarriagePayment, Report, ReceivedCash, CamOrder
}
