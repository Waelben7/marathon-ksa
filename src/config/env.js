export const PORT = process.env.PORT || 3000
export const HOST = process.env.HOST || 'localhost'
export const NODE_ENV = process.env.NODE_ENV || 'development'
export const DB = process.env.MONGODB_URI || 'mongodb://rexrproject:rexrizO7N1jX!@51.77.195.204:27017/rexrproject'
// export const DB = process.env.MONGODB_URI || 'mongodb://localhost:27017/rexrproject'
export const SECRET = process.env.SECRET || 'this!!isaverysecure@stringtonotbeingha%cked'

export const RHOST = process.env.RHOST
export const RDB = process.env.RDB 
export const RUSER = process.env.RUSER 
export const RPASS = process.env.RPASS

export const DHOST = process.env.DHOST
export const DDB = process.env.DDB 
export const DUSER = process.env.DUSER
export const DPASS = process.env.DPASS 

export const PRIVKEY = process.env.PRIVKEY
export const FULLCHAIN = process.env.FULLCHAIN