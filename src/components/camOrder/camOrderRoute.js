import { requireAuth } from '../../services/authServices'
import { create, getAll, update, postComment } from './camOrderController'

export default function (router) {
  router.get('/camOrders', requireAuth, getAll)
  router.post('/camOrder', create)
  router.put('/camOrder/:id', requireAuth, update)
  router.post('/camOrder/:id/comment', requireAuth, postComment)
}
