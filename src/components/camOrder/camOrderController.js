import { CamOrder } from '../../config/models'

export async function getAll(req, res) {
  try {

    const camOrders = await CamOrder.find()
    return res.json({ camOrders })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function create(req, res) {
  try {

    const order = await CamOrder.create(req.body)
    return res.json(order)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function update(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Order ID cannot be empty' })

    let camOrder = await CamOrder.update({_id: req.params.id}, {$set: req.body})

    return res.json(camOrder)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function postComment(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Order ID cannot be empty' })

    let camOrder = await CamOrder.findOne({ _id: req.params.id })

    const comment = {
      user: req.user.name,
      date: new Date(),
      text: req.body.text
    }

    camOrder.comments.push(comment)

    await camOrder.save()

    return res.json({comment})

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}