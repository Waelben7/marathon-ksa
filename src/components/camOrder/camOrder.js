import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import autoIncrement from 'mongoose-auto-increment'

const camOrderSchema = new mongoose.Schema(
  {
    number: { type: Number },
    fullName: { type: String },
    quantity: { type: Number },
    phone: { type: String },
    address1: { type: String },
    address2: { type: String },
    remarks: { type: String },
    comments: [{
      user: { type: String },
      date: { type: Date },
      text: { type: String }
    }]
  },
  {
    timestamps: true,
    usePushEach: true
  }
)

camOrderSchema.plugin(autoIncrement.plugin, { model: 'camOrder', field: 'number' });
camOrderSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })
export default mongoose.model('camOrder', camOrderSchema)