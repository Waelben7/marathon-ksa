import { backupFromRoute } from '../../cron/databaseBackup'
import {reverseSheet, balancePerRegion, fixBalnces, checkDriverBalance} from './sysAdminController'

export default function (router) {
  router.post('/backupFromRoute', backupFromRoute)
  router.post('/reversecSheet', reverseSheet)
  router.post('/balancePerRegion', balancePerRegion)
  router.post('/fixBalnces', fixBalnces)
  router.post('/checkDriverBalance/:carriageId', checkDriverBalance)
}
