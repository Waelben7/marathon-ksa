import { Transaction, Driver } from '../../config/models'

export async function reverseSheet(req, res) {
  try {
    console.log(req.body)

    let transactions = await Transaction.find({
      orderId: {
        $in: req.body.orderIds
      }
    })

    for (let transaction of transactions) {
      let driver = await Driver.findOne({ _id: transaction.driverModelId })
      driver.balance -= transaction.balance

      await Transaction.delete({ _id: transaction.id })
      await driver.save()
    }

    return res.json({ success: true })
  } catch (error) {

    return res.json({ success: false, error: error })
  }
}

export async function balancePerRegion(req, res) {
  try {

    let drivers = await Driver.find()

    let AlhassaBalance = 0
    let EasternRegionBalance = 0

    for (let driver of drivers) {

      if (!driver.branch)
        continue

      if (driver.branch == 'Alhassa')
        AlhassaBalance += driver.balance
      else if (driver.branch == 'Eastern Region')
        EasternRegionBalance += driver.balance
      else
        return res.json({ de5lafi7it: true })
    }

    return res.json({ AlhassaBalance, EasternRegionBalance })

  } catch (error) {

    console.log(error)
    return res.json({ success: false, error: error })
  }
}

export async function fixBalnces(req, res) {
  try {

    req.setTimeout(10 * 60 * 1000)

    let drivers = await Driver.find()
    let totalAffected = 0

    for (let driver of drivers) {
      const transactions = await Transaction.find({ driverModelId: driver._id })
      let sum = transactions.reduce((s, item) => s + item.balance, 0)

      sum = sum.toFixed(2)
      driver.balance = driver.balance.toFixed(2)

      if (sum != driver.balance) {
        const ch = '\n-------------------------\n' +
          'Balance check error for driver ' + driver.captainId +
          ':\nDriver registred balance ' + driver.balance +
          '\nCalculated balance ' + sum +
          '\nDifference ' + (driver.balance - sum).toFixed(2)

        console.log(ch)

        driver.balance = parseFloat(sum)
        await driver.save()

        totalAffected++
      }

      console.log('Finish working with driver ', driver.captainId)
    }

    return res.json({ success: true, totalAffected })

  } catch (error) {

    console.log(error)
    return res.json({ success: false, error: error })
  }
}

export async function checkDriverBalance(req, res) {
  try {

    // req.setTimeout(10 * 60 * 1000)

    const driver = await Driver.findOne({ carriageId: req.params.carriageId })
    const transactions = await Transaction.find({
      driverModelId: driver._id,
      deleted: false
    })

    let data = []

    let start = null
    let end = null
    let ordersCount = 0
    let balance = 0
    let oldRemaining = 0
    let totalToPay = 0
    let paid = 0
    let newRemaining = 0


    for (let transaction of transactions) {

      if (transaction.transactionType == 'استحقاق') {
        if (ordersCount == 0)
          start = transaction.transactionDate

        balance += transaction.balance
        ordersCount++

      } else if (transaction.transactionType == 'دفع') {

        end = transaction.transactionDate
        totalToPay = balance + oldRemaining
        paid = transaction.balance
        newRemaining = totalToPay + paid

        data.push({
          start,
          end,
          ordersCount,
          balance,
          oldRemaining,
          totalToPay,
          paid,
          newRemaining
        })

        oldRemaining = newRemaining
        newRemaining = 0
        ordersCount = 0
        balance = 0

        start = null
        end = null
        totalToPay = 0
        paid = 0
        newRemaining = 0

      }

    }

    return res.json({ data })

  } catch (error) {

    console.log(error)
    return res.json({ success: false, error: error })
  }
}
