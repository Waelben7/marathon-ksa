import { requireAuth } from '../../services/authServices'
import { getAll } from './receivedCashController'

export default function (router) {

  router.get('/receivedCash', requireAuth, getAll)

}
