import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'

const receivedCashSchema = new mongoose.Schema(
  {
    date: { type: Date },
    branch: { type: String },
    amount: { type: Number }
  },
  {
    timestamps: true
  }
)

receivedCashSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })

export default mongoose.model('receivedCash', receivedCashSchema)