import { ReceivedCash } from '../../config/models'

export async function getAll(req, res) {
  try {

    let options = {}
    if (req.user.branch) options.branch = req.user.branch

    const receivedCash = await ReceivedCash.find(options).sort({date: -1})

    return res.json({ receivedCash })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}