import { requireAuth } from '../../services/authServices'
import { getAll, importDemandedPayments, verifyFile, create, remove, update } from './carriagePaymentController'

export default function (router) {

  router.get('/carriagePayments', requireAuth, getAll)
  router.post('/carriagePayments', requireAuth, create)
  router.post('/carriagePayments/importFile', requireAuth, importDemandedPayments)
  router.post('/carriagePayments/verifyFile', requireAuth, verifyFile)
  router.put('/carriagePayments/:id', update)
  router.delete('/carriagePayments/:id', remove)
}
