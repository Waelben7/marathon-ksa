import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import autoIncrement from 'mongoose-auto-increment'

const carriagePaymentSchema = new mongoose.Schema(
  {
    receiptNo: { type: Number },
    date: { type: Date },
    type: { type: String },
    amount: { type: Number },
    comments: { type: String }
  },
  {
    timestamps: true
  }
)

carriagePaymentSchema.plugin(autoIncrement.plugin, { model: 'carriagePayment', field: 'receiptNo' });
carriagePaymentSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })

export default mongoose.model('carriagePayment', carriagePaymentSchema)