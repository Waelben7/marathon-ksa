import fs from 'fs'
import _ from 'lodash'
import path from 'path'
import csv from 'csvtojson'
import { CarriagePayment } from '../../config/models'

export async function importDemandedPayments(req, res) {
  try {

    const filePath = path.join(__dirname, '/../../../public/uploads/' + req.body.fileName)
    const carriagePayments = await csv().fromFile(filePath)
    let carriagePayment
    await fs.unlinkSync(filePath)

    for (let item of carriagePayments) {

      if (item.field1 == 'Total Result') {
        const dateTab = req.body.fileName.split('_')[1].split(' ')[2].split('.')[0].split('-')
        let date = new Date(dateTab[2] + '-' + dateTab[1] + '-' + dateTab[0])
        date.setHours(date.getHours() + 1)

        carriagePayment = {
          date,
          type: 'Demanded',
          amount: item.field3
        }

        carriagePayment = await CarriagePayment.create(carriagePayment)
      }
    }

    return res.json(carriagePayment)

  } catch (error) {

    console.log(error)

    if (error.message.includes('File does not exist'))
      return res.json({ error: 'wrong file name' }).status(500)

    return res.json({ error: error.message }).status(500)
  }
}

export async function verifyFile(req, res) {
  try {

    const filePath = path.join(__dirname, '/../../../public/uploads/' + req.body.fileName)
    const carriagePayments = await csv().fromFile(filePath)
    let correctFile = false

    for (let item of carriagePayments)
      if (item.field1 == 'Total Result')
        correctFile = true

    return res.json({ correctFile })

  } catch (error) {

    console.log(error)

    if (error.message.includes('File does not exist'))
      return res.json({ error: 'wrong file name' }).status(400)

    return res.json({ error: error.message }).status(500)
  }
}

export async function getAll(req, res) {
  try {

    let totalDemanded = 0, totalAffected = 0

    const carriagePayments = await CarriagePayment.find()

    for (let item of carriagePayments)
      if (item.type == 'Affected')
        totalAffected += item.amount
      else
        totalDemanded += item.amount

    return res.json({ carriagePayments, totalAffected, totalDemanded })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function create(req, res) {
  try {
    const carriagePayment = await CarriagePayment.create({
      date: new Date(req.body.date),
      type: req.body.type,
      amount: req.body.amount,
      comments: req.body.comments
    })

    return res.json(carriagePayment)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function remove(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Payment id cannot be empty' })

    await CarriagePayment.delete({ _id: req.params.id })

    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function update(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Payment id cannot be empty' })

    await CarriagePayment.update({ _id: req.params.id }, { $set: req.body })

    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}
