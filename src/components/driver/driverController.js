import _ from 'lodash'
import { Driver, Transaction } from '../../config/models'
import firebase from '../../config/fcm'

export async function get(req, res) {
  try {

    let options = { registredFrom: { $ne: 'link' } }

    if (req.user.branch) options.branch = req.user.branch

    if (req.params.registredFrom) options.registredFrom = req.params.registredFrom

    const drivers = await Driver.find(options).sort('captainId')

    return res.json({ drivers })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function getOne(req, res) {
  try {

    let options = { captainId: req.params.captainId }
    if (req.user.branch) options.branch = req.user.branch

    let driver = await Driver.findOne(options)

    const transactions = await Transaction.find({ driverModelId: driver._id })
    const sum = transactions.reduce((s, item) => s + item.balance, 0)
    driver.balance = sum.toFixed(2)

    return res.json({ driver })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function getByNationalId(req, res) {
  try {

    let driver = await Driver.findOne({ captainIdentityNumber: req.params.identityNumber })
      .select('carriageId captainId firstName lastName balance status branch _id')

    if (!driver)
      return res.status(400).json({ error: "Wrong national id" })

    driver = driver.toJSON()

    const transactions = await Transaction.find({ driverModelId: driver._id })
    const sum = transactions.reduce((s, item) => s + item.balance, 0)
    driver.balance = sum.toFixed(2)

    if (!driver.carriageId)
      driver.carriageId = 'empty'

    if (!driver.branch)
      driver.branch = 'empty'

    return res.json(driver)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function create(req, res) {
  try {

    let driver = await Driver.findOne({ captainIdentityNumber: req.body.captainIdentityNumber })

    if (driver)
      return res.status(400).json({ code: 121, error: 'Driver already exist' })

    driver = await Driver.create(req.body)
    return res.json({ driver })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function update(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Captin ID cannot be empty' })

    await Driver.update({ _id: req.params.id }, { $set: req.body })
    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function remove(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Captin ID cannot be empty' })

    console.log(req.user)

    await Driver.delete({ _id: req.params.id })
    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function updateFCM(req, res) {
  try {
    if (!req.params.identityNumber)
      return res.status(400).json({ code: 117, error: 'Identity number cannot be empty' })

    if (!req.body.fcm)
      return res.status(400).json({ code: 117, error: 'FCM cannot be empty' })

    await Driver.update({ captainIdentityNumber: req.params.identityNumber }, { $set: { fcm: req.body.fcm } })
    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function sendMessage(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Driver ID cannot be empty' })

    let driver = await Driver.findOne({ _id: req.params.id })

    if (!driver.fcm)
      return res.status(400).json({ code: 117, error: 'Driver FCM not found' })

    const message = {
      notification: {
        title: req.body.title,
        body: req.body.body
      },
      token: driver.fcm
    }

    await firebase.messaging().send(message)

    driver.messages.push({
      title: req.body.title,
      body: req.body.body,
      date: new Date()
    })

    await driver.save()

    return res.json(driver.messages.pop())

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function sendMessageToAll(req, res) {
  try {

    res.status(204).end()

    let drivers = await Driver.find({ fcm: { $ne: null } })

    for (let driver of drivers) {

      const message = {
        notification: {
          title: req.body.title,
          body: req.body.body
        },
        token: driver.fcm
      }

      try {

        await firebase.messaging().send(message)

        driver.messages.push({
          title: req.body.title,
          body: req.body.body,
          date: new Date()
        })

        await driver.save()

      } catch (error) {
        console.log('error while send messeage to driver: ' + driver.captainId)
        console.log(message)
        console.log(error)
      }

    }

  } catch (error) {
    console.log(error)
  }
}

export async function getMessages(req, res) {
  try {

    const driver = await Driver.findOne({ captainId: req.params.captainId })
    const messages = driver.messages.map(msg => {
      msg = msg.toJSON()
      msg.date = new Date(msg.date).toISOString().substr(0, 16).replace('T', ' ').replace(/-/g, '/')
      return msg
    })

    return res.json(messages)

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function deleteMessage(req, res) {
  try {
    if (!req.params.driverId)
      return res.status(400).json({ code: 117, error: 'Driver ID cannot be empty' })

    let driver = await Driver.findOne({ _id: req.params.driverId })

    for (let i in driver.messages) {
      if (driver.messages[i]._id == req.params.messageId) {
        driver.messages.splice(i, 1)
        break
      }
    }

    await driver.save()

    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}