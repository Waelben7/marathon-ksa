import mongoose from 'mongoose'
import connection from './../../config/database'
import mongooseDelete from 'mongoose-delete'
import autoIncrement from 'mongoose-auto-increment'

autoIncrement.initialize(connection);

const driverSchema = new mongoose.Schema(
  {
    captainId: { type: Number },
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String },
    password: { type: String },
    picture: { type: String },

    carriageId: { type: Number },
    branch: { type: String },
    phoneNumber: { type: String },
    dateOfBirth: { type: Date },
    address: { type: String },
    nationality: { type: String },
    bankName: { type: String },
    iban: { type: String },
    balance: { type: Number, default: 0 },

    status: { type: String },
    comments: { type: String },
    registrationDate: { type: Date },

    captainIdentityNumber: { type: String, unique: true },
    identityDate: { type: Date },

    custodyDate: { type: Date },
    hasTShirt: { type: Boolean },
    hasBoxCold: { type: Boolean },
    hasBoxHot: { type: Boolean },
    wire: { type: Boolean },

    identityIqama: { type: String },
    drivingLicense: { type: String },

    fcm: { type: String },
    messages: [{
      title: { type: String },
      body: { type: String },
      date: {type: Date}
    }],

    registredFrom: { type: String, default: 'dashboard' }
  },
  {
    timestamps: true,
    usePushEach: true
  }
)

driverSchema.plugin(autoIncrement.plugin, { model: 'driver', field: 'captainId' });
driverSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })

export default mongoose.model('driver', driverSchema)