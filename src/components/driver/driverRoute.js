import { requireAuth } from '../../services/authServices'
import {
  create, get, update, remove, getOne, getByNationalId,
  updateFCM, sendMessage, getMessages, deleteMessage, sendMessageToAll
} from './driverController'

export default function (router) {
  router.get('/drivers/:registredFrom?', requireAuth, get)

  router.get('/driver/byNationalId/:identityNumber', getByNationalId)
  router.get('/driver/:captainId', requireAuth, getOne)

  router.post('/driver', create)
  router.put('/driver/:id', requireAuth, update)
  router.delete('/driver/:id', requireAuth, remove)

  router.put('/driver/:identityNumber/fcm', updateFCM)

  router.get('/driver/:captainId/messages', getMessages)
  router.post('/driver/:id/message', sendMessage)
  router.post('/driver/messageToAll', sendMessageToAll)
  router.delete('/driver/:driverId/message/:messageId', deleteMessage)
}
