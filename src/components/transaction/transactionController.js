// import fs from 'fs'
import _ from 'lodash'
import path from 'path'
import csv from 'csvtojson'
import { Transaction, Driver, Report, ReceivedCash } from '../../config/models'

function getTransactionIdByOrderId(transactions, orderId) {
  for (let transaction of transactions)
    if (transaction.orderId == orderId)
      return transaction._id
  return -1
}

function getDriverByCarriageId(drivers, carriageId) {
  for (let driver of drivers)
    if (driver.carriageId == carriageId)
      return driver
  return -1
}

export async function importTransaction(req, res) {
  try {

    const filePath = path.join(__dirname, '/../../../public/uploads/' + req.body.fileName)
    let transactions = await csv().fromFile(filePath)
    let correctFile = true
    let notFoundDrivers = []
    let sheetDrivers = []
    let driverFees

    const stage0start = new Date()

    console.log('transactions.length : ' + transactions.length)
    transactions = transactions.filter(item => !(isNaN(item['Driver ID'])))
    console.log('transactions.length after removing empty driver ids : ' + transactions.length)

    const transaction = await Transaction.findOne({
      orderId: transactions[transactions.length - 1]['Order Id']
    })

    if (transaction)
      return res.json({ correctFile: false, error: 'File already uploaded' })

    for (let transaction of transactions) {
      const driverId = parseInt(transaction['Driver ID'])

      if (!sheetDrivers.includes(driverId))
        sheetDrivers.push(driverId)
    }

    console.log('sheetDrivers.length : ' + sheetDrivers.length)

    let BDDrivers = await Driver.find({ carriageId: { $in: sheetDrivers } })

    if (BDDrivers.length < sheetDrivers.length) {
      console.log(BDDrivers.length + '   -   ' + sheetDrivers.length)

      const BDDriversIds = BDDrivers.map(item => item.carriageId)
      notFoundDrivers = sheetDrivers.filter(item => !BDDriversIds.includes(item))

      correctFile = false
      return res.json({ correctFile, error: 'NotIsSystemDrivers', notFoundDrivers })
    }

    console.log('Stage 0 completed !! in ' + (new Date() - stage0start) / 1000 + ' s ---------------------------------------------------------------------------')




    const stage1start = new Date()

    for (let i = 0; i < transactions.length; i++) {
      // const startDate = new Date()

      let driver = getDriverByCarriageId(BDDrivers, transactions[i]['Driver ID'])

      if (driver == -1) {
        console.log(BDDrivers, transactions[i]['Driver ID'])
        continue
      }

      if (driver.branch == 'Eastern Region')
        driverFees = 14
      else
        driverFees = 14

      transactions[i].transactionType = 'استحقاق'
      transactions[i].transactionDate = new Date(transactions[i].Date + ' ' + transactions[i]['Placement Time'].replace(/\s+/g, ''))
      transactions[i].status = transactions[i]['Order Status']
      transactions[i].amount = parseFloat(transactions[i].Amount)
      transactions[i].paymentMethod = transactions[i]['Payment Type']

      if (transactions[i].status == 'bad_order')
        transactions[i].balance = -driverFees
      else if (transactions[i].status == 'canceled')
        transactions[i].balance = 0
      else if (transactions[i].paymentMethod == 'Credit Card' || transactions[i].paymentMethod == 'Apple Pay')
        transactions[i].balance = -driverFees
      else if (transactions[i].paymentMethod == 'Cash on Delivery')
        transactions[i].balance = transactions[i].amount - driverFees
      else
        return res.status(400).json({ error: 'not recognised payment method' })

      transactions[i].driverModelId = driver._id
      transactions[i].orderId = transactions[i]['Order Id']
      transactions[i].referance = req.body.fileName.split('_')[1]

      driver.balance = parseFloat(driver.balance + transactions[i].balance)
      await driver.save()

      // console.log('Transaction num ' + i + ' order id ' + transactions[i]['Order Id'] + ' finished in ' + (new Date() - startDate) / 1000 + ' s')
    }

    console.log('Stage 1 completed !! in ' + (new Date() - stage1start) / 1000 + ' s ---------------------------------------------------------------------------')




    const stage2start = new Date()

    console.log('Stage 2 completed !! in ' + (new Date() - stage2start) / 1000 + ' s ---------------------------------------------------------------------------')




    const stage3start = new Date()

    transactions = await Transaction.create(transactions)

    console.log('Stage 3 completed !! in ' + (new Date() - stage3start) / 1000 + ' s ---------------------------------------------------------------------------')



    const stage4start = new Date()

    let reports = await Report.find({ order: null, status: 'pending' })

    for (let report of reports) {
      const transactionId = getTransactionIdByOrderId(transactions, report.orderId)
      if (transactionId != -1) {
        report.order = transactionId
        await report.save()
      }
    }

    console.log('Stage 4 completed !! in ' + (new Date() - stage4start) / 1000 + ' s ---------------------------------------------------------------------------')




    return res.json({ correctFile })

  } catch (error) {

    console.log(error)

    if (error.message.includes('File does not exist'))
      return res.status(500).json({ error: 'wrong file name' })

    return res.status(500).json({ error: error.message })
  }
}

export async function manualOrder(req, res) {
  try {

    let transaction = req.body

    transaction.transactionDate = new Date()
    transaction.transactionType = 'استحقاق'
    transaction.referance = 'Manual Order from ' + req.user.name

    let driver = await Driver.findOne({ _id: transaction.driverModelId })
    const driverFees = driver.branch == 'Eastern Region' ? 14 : 14

    if (transaction.status == 'bad_order')
      transaction.balance = -driverFees
    else if (transaction.status == 'canceled')
      transaction.balance = 0
    else if (transaction.paymentMethod == 'Credit Card' || transaction.paymentMethod == 'Apple Pay')
      transaction.balance = -driverFees
    else if (transaction.paymentMethod == 'Cash on Delivery')
      transaction.balance = transaction.amount - driverFees
    else
      return res.status(400).json({ error: 'not recognised payment method' })

    driver.balance = parseFloat(driver.balance + transaction.balance)
    await driver.save()

    transaction = await Transaction.create(transaction)

    let reports = await Report.find({ orderId: transaction.orderId, status: 'pending' })

    for (let report of reports) {
      report.order = transaction._id
      await report.save()
    }

    return res.json(transaction)

  } catch (error) {

    console.log(error)
    return res.status(500).json({ error: error.message })
  }
}

export async function verifyFile(req, res) {
  try {

    const filePath = path.join(__dirname, '/../../../public/uploads/' + req.body.fileName)
    let transactions = await csv().fromFile(filePath)
    let correctFile = true
    let notFoundDrivers = []

    for (let item of transactions) {
      const driver = await Driver.findOne({ carriageId: item['Driver ID'] })
      const transaction = await Transaction.findOne({ orderId: item['Order Id'] })

      if (transaction) {
        console.log(transaction.orderId)
        return res.json({ correctFile: false, error: 'File already uploaded' })
      }

      if (!driver && notFoundDrivers.indexOf(item['Driver ID']) < 0) {
        notFoundDrivers.push(item['Driver ID'])
        correctFile = false
      }
    }

    if (notFoundDrivers.length > 0)
      return res.json({ correctFile, error: 'NotIsSystemDrivers', notFoundDrivers })

    return res.json({ correctFile })

  } catch (error) {

    console.log(error)

    if (error.message.includes('File does not exist'))
      return res.status(400).json({ error: 'wrong file name' })

    return res.status(500).json({ error: error.message })
  }
}

export async function getByDriver(req, res) {
  try {

    let options = { captainId: req.params.driverId }
    if (req.user.branch) options.branch = req.user.branch

    const driver = await Driver.findOne(options)

    if (!driver)
      return res.status(400).json({ error: 'wrong driver id' })

    let transactions = await Transaction.find({ driverModelId: driver._id })

    let result = []

    let ordersCount = 0
    let balance = 0
    let oldRemaining = 0
    let totalToPay = 0
    let paid = 0
    let newRemaining = 0


    for (let transaction of transactions) {

      transaction = transaction.toJSON()

      if (transaction.transactionType == 'استحقاق') {

        balance += transaction.balance
        ordersCount++

      } else if (transaction.transactionType == 'دفع') {

        totalToPay = balance + oldRemaining
        paid = transaction.balance
        newRemaining = totalToPay + paid

        transaction.details = {
          ordersCount,
          balance,
          oldRemaining,
          totalToPay,
          paid,
          newRemaining
        }

        oldRemaining = newRemaining
        newRemaining = 0
        ordersCount = 0
        balance = 0
        totalToPay = 0
        paid = 0
      }

      result.push(transaction)
    }

    return res.json({ transactions: result })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function getByStatus(req, res) {
  try {

    let transactions = await Transaction.find({ status: req.params.status }).populate('driverModelId')

    if (req.user.branch)
      transactions = transactions.filter(transaction => transaction.driverModelId.branch == req.user.branch)

    return res.json({ transactions })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function getByType(req, res) {
  try {

    const transactions = await Transaction.find({ transactionType: req.params.type }).populate('driverModelId')

    return res.json({ transactions })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function payment(req, res) {
  try {

    let driver = await Driver.findOne({ carriageId: req.body.carriageId })
    const lastTransaction = await Transaction.findOne({ driverModelId: driver._id }, {}, { sort: { 'createdAt': -1 } })

    if (lastTransaction) {
      const timeFromLastTransaction = new Date() - lastTransaction.createdAt

      if (timeFromLastTransaction < 30000)  //30 seconds
        return res.status(400).json({
          code: 113,
          error: "Please wait " + parseInt((30000 - timeFromLastTransaction) / 1000) + " seconds to effect your next payment"
        })
    }

    req.body.amount = parseFloat(req.body.amount)

    const transaction = {
      driverModelId: driver._id,
      amount: -req.body.amount,
      transactionDate: new Date(),
      transactionType: 'دفع',
      paymentMethod: req.user.name,
      balance: -req.body.amount,
      referance: 'محصل: تحصيل مكتبي'
    }

    await Transaction.create(transaction)

    const oldDriverBalance = parseFloat(driver.balance)
    driver.balance = parseFloat(oldDriverBalance - req.body.amount)

    if (oldDriverBalance == driver.balance)
      console.log('balance bug =>' + new Date() + ' before save old balance: ' + oldDriverBalance + ' new calculated: ' + driver.balance)

    await driver.save()

    if (oldDriverBalance == driver.balance)
      console.log('balance bug => after save old balance: ' + oldDriverBalance + ' new saved: ' + driver.balance)

    let date = new Date(transaction.transactionDate)
    date.setHours(date.getHours() + 3)
    date = date.toISOString().slice(0, 10)

    const branch = driver.branch

    let record = await ReceivedCash.findOne({ date, branch })

    if (record) {
      record.amount += req.body.amount
      await record.save()
    } else {
      record = { date, branch, amount: req.body.amount }
      await ReceivedCash.create(record)
    }

    return res.status(200).end()

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function updateStatus(req, res) {
  try {
    if (!req.params.orderId)
      return res.status(400).json({ code: 117, error: 'orderId cannot be empty' })

    let driverFees = 14
    let transaction = await Transaction.findOne({ orderId: req.params.orderId })
    let driver = await Driver.findOne({ _id: transaction.driverModelId })

    if (driver.branch == 'Eastern Region')
      driverFees = 14

    switch (req.body.status) {
      case 'bad_order':
        // this work for credit card and canceled use case
        driver.balance = parseFloat(driver.balance - transaction.balance - driverFees)
        transaction.balance = -driverFees
        transaction.status = 'bad_order'
        break;

      case 'canceled':
        driver.balance = parseFloat(driver.balance - transaction.balance)
        transaction.balance = 0
        transaction.status = 'canceled'
        break;

      case 'pending':
        driver.balance = parseFloat(driver.balance - transaction.balance)
        transaction.balance = 0
        transaction.status = 'pending'
        break;

      case 'delivered':
        let amount = (transaction.amount - driverFees)

        if (transaction.paymentMethod == 'Credit Card' || transaction.paymentMethod == 'Apple Pay')
          amount = -driverFees

        driver.balance = parseFloat(driver.balance - transaction.balance + amount)
        transaction.balance = amount
        transaction.status = 'delivered'
        break;

      default:
        return res.status(400).json({ error: 'wrong order status' })
        break;
    }

    await transaction.save()
    await driver.save()

    return res.json({ driverBalance: driver.balance, transactionBalance: transaction.balance })

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function remove(req, res) {
  try {
    if (!req.params.id)
      return res.status(400).json({ code: 117, error: 'Captin ID cannot be empty' })

    const transaction = await Transaction.findOne({ _id: req.params.id })
    let driver = await Driver.findOne({ _id: transaction.driverModelId })

    driver.balance -= transaction.balance

    await Transaction.delete({ _id: req.params.id }, req.user._id)
    await driver.save()

    // const branch = driver.branch

    // let record = await ReceivedCash.findOne({ date, branch })

    // if (record) {
    //   record.amount += req.body.amount
    //   await record.save()
    // } else {
    //   record = { date, branch, amount: req.body.amount }
    //   await ReceivedCash.create(record)
    // }

    return res.status(204).end()

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function transfer(req, res) {
  try {
    if (!req.body.orderId)
      return res.status(400).json({ code: 117, error: 'orderId cannot be empty' })

    if (!req.body.carriageId)
      return res.status(400).json({ code: 117, error: 'carriageId cannot be empty' })

    let transaction = await Transaction.findOne({ orderId: req.body.orderId })
    let originDriver = await Driver.findOne({ _id: transaction.driverModelId })
    let newDriver = await Driver.findOne({ carriageId: req.body.carriageId })

    transaction.driverModelId = newDriver._id
    newDriver.balance += transaction.balance
    originDriver.balance -= transaction.balance

    await transaction.save()
    await originDriver.save()
    await newDriver.save()

    return res.json({ driverBalance: originDriver.balance })

  } catch (error) {

    console.log(error)
    if (error.name == 'CastError')
      return res.status(400).json({ error: error.message })

    return res.status(500).end()
  }
}

export async function getReceivedCashTransactions(req, res) {
  try {

    const record = await ReceivedCash.findOne({ _id: req.params.receivedCashId })

    let gteDate = new Date(record.date)
    gteDate.setHours(gteDate.getHours() - 3)

    let lteDate = new Date(record.date)
    lteDate.setDate(lteDate.getDate() + 1)
    lteDate.setHours(lteDate.getHours() - 3)

    let transactions = await Transaction.find({
      transactionDate: {
        $gte: gteDate,
        $lt: lteDate
      },
      transactionType: 'دفع'

    }).populate('driverModelId')
      .sort({ transactionDate: -1 })

    transactions = transactions.filter(item => item.driverModelId.branch == record.branch)

    return res.json({ transactions })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}