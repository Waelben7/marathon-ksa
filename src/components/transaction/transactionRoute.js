import { requireAuth } from '../../services/authServices'
import {
  getByDriver, getByStatus, getByType, importTransaction, verifyFile, payment, updateStatus,
  transfer, remove, getReceivedCashTransactions, manualOrder
} from './transactionController'

export default function (router) {

  router.get('/transactions/byType/:type', requireAuth, getByType)
  router.get('/transactions/byStatus/:status', requireAuth, getByStatus)
  router.get('/transactions/byDriver/:driverId', requireAuth, getByDriver)
  router.get('/transactions/receivedCash/:receivedCashId', requireAuth, getReceivedCashTransactions)

  router.post('/transaction/importFile', requireAuth, importTransaction)
  router.post('/transaction/verifyFile', requireAuth, verifyFile)
  router.post('/transaction/payment', requireAuth, payment)
  router.post('/transaction/manualOrder', requireAuth, manualOrder)

  router.put('/transaction/transfer', requireAuth, transfer)
  router.put('/transaction/:orderId', requireAuth, updateStatus)
  
  router.delete('/transaction/:id', requireAuth, remove)
}
