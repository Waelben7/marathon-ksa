import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'

const transactionSchema = new mongoose.Schema(
  {
    orderId: { type: Number },
    driverModelId: { type: mongoose.Schema.Types.ObjectId, ref: 'driver' },
    status: { type: String },
    amount: { type: Number },
    transactionDate: { type: Date },
    transactionType: { type: String },
    paymentMethod: { type: String },
    balance: { type: Number },
    referance: { type: String }
  },
  {
    timestamps: true
  }
)

transactionSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })

export default mongoose.model('transaction', transactionSchema)