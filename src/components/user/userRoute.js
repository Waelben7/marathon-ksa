import { requireAuth } from '../../services/authServices'
import { update, get } from './userController'

export default function (router) {
    router.get('/user/current', requireAuth, get)
    router.put('/user/changePassword', requireAuth, update)
}
