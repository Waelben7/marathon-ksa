import _ from 'lodash'
import crypto from 'crypto'
import { User } from '../../config/models'

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export function signIn(req, res) {
  const user = _.pick(req.user, 'name','token')
  return res.json({ user: user })
}

export async function signUp(req, res) {
  try {
    let user = _.pick(req.body, 'name', 'email', 'password')

    if (!user.email)
      return res.status(400).json({ code: 102, error: 'Email cannot be empty' })

    if (!emailRegex.test(user.email))
      return res.status(400).json({ code: 101, error: 'Wrong email form' })

    const existUsers = await User.find({ email: user.email })

    if (existUsers.length)
      return res.status(400).json({ code: 102, error: 'Email already used' })

    if (!user.password)
      return res.status(400).json({ code: 103, error: 'Password cannot be empty' })

    if (user.password.length < 8)
      return res.status(400).json({ code: 104, error: 'Password should contain eight characters or more' })

    if (!user.name)
      return res.status(400).json({ code: 105, error: 'Name cannot be empty' })

    const token = crypto.createHash('sha256').update(crypto.randomBytes(48).toString('hex')).digest('hex')
    user.token = token
    await User.create(user)

    user = _.pick(user, 'name')
    return res.json({ user: user })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function signOut(req, res) {
  try {
    delete req.user.token
    delete req.session.token
    await req.user.save()

    return res.status(204).end()

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}
