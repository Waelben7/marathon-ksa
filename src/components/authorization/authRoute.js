import { requireAuth, shouldSignIn } from '../../services/authServices'
import { signIn, signUp, signOut } from './authController'

export default function (router) {
  router.post('/signUp', signUp)
  router.post('/signIn', shouldSignIn, signIn)
  router.put('/signOut', requireAuth, signOut)
}
