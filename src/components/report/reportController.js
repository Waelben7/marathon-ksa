import _ from 'lodash'
import { Report, Driver, Transaction } from '../../config/models'

export async function getAll(req, res) {
  try {

    let reports = await Report.find().populate('driver').populate('order')

    if (req.user.branch)
      reports = reports.filter(report => report.driver.branch == req.user.branch)

    return res.json({ reports })

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function create(req, res) {
  try {

    let report = await Report.findOne({ orderId: req.body.orderId })
    if (report)
      return res.status(400).json({ error: 'You have already reported this order' })

    const transaction = await Transaction.findOne({ orderId: req.body.orderId })
    if (transaction)
      req.body.order = transaction._id

    const driver = await Driver.findOne({ captainIdentityNumber: req.body.nationalId })
    req.body.driver = driver._id
    report = await Report.create(req.body)

    return res.json(report)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}

export async function update(req, res) {
  try {

    let report = await Report.findOne({ _id: req.params.id })
    if (!report)
      return res.status(400).json({ error: 'Wrong report ID' })

    report.status = req.body.status
    report.response = req.body.response

    await report.save()

    return res.json(report)

  } catch (error) {

    console.log(error)
    return res.status(500).end()
  }
}
