import { requireAuth } from '../../services/authServices'
import { getAll, create, update } from './reportController'

export default function (router) {

  router.post('/report', create)
  router.get('/reports', requireAuth, getAll)
  router.put('/report/:id', requireAuth, update)
}
