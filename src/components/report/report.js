import mongoose from 'mongoose'
import mongooseDelete from 'mongoose-delete'
import autoIncrement from 'mongoose-auto-increment'

const reportSchema = new mongoose.Schema(
  {
    number: { type: Number },
    driver: { type: mongoose.Schema.Types.ObjectId, ref: 'driver' },
    order: { type: mongoose.Schema.Types.ObjectId, ref: 'transaction' },
    orderId: { type: Number },
    message: { type: String },
    status: { type: String, default: 'pending' }, // pending, in review, done
    orderStatus: { type: String },
    response: { type: String }
  },
  {
    timestamps: true
  }
)

reportSchema.plugin(autoIncrement.plugin, { model: 'report', field: 'number' });
reportSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })
export default mongoose.model('report', reportSchema)

