import { PORT, NODE_ENV, SECRET, PRIVKEY, FULLCHAIN, HOST } from './config/env'
import mongooseConnection from './config/database'
import connectMongo from 'connect-mongo'
import session from 'express-session'
import bodyParser from 'body-parser'
import routes from './config/routes'
import express from 'express'
import morgan from 'morgan'
import https from 'https'
import http from 'http'
import path from 'path'
import './cron/cronJob'
import fs from 'fs'

const app = express()
const mongoStore = connectMongo(session)
const serverHttp = http.createServer(app)
let serverHttps

if (NODE_ENV != 'development') {
  serverHttps = https.createServer({
    key: fs.readFileSync(PRIVKEY),
    cert: fs.readFileSync(FULLCHAIN)
  }, app)
}

let sess = {
  secret: SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {},
  store: new mongoStore({ mongooseConnection: mongooseConnection })
}

app.use(session(sess))
app.use(bodyParser.json({ limit: "4mb" }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use('/', routes)

app.use('/', express.static(path.join(__dirname, '/../public/uploads')));
app.use('/', express.static(path.join(__dirname, '/../public/client')));
app.use('/dashboard', express.static(path.join(__dirname, '/../public/dashboard')));

if (NODE_ENV != 'development') {

  http.createServer((req, res) => {
    res.writeHead(307, { 'Location': 'https://' + HOST + req.url })
    res.end()
  }).listen(80, () => console.log('start redirection server on port 80'))

  serverHttps.listen(443, () => console.log('start in ' + NODE_ENV + ' environment on port 443'))

} else {
  serverHttp.listen(PORT, () => console.log('start in ' + NODE_ENV + ' environment on port ' + PORT))
}