var transactions = db.transactions.find({
  transactionType: 'دفع',
  // createdAt: {
  //   $gte: new Date("2018-12-02"),
  //   $lte: new Date("2018-12-04")
  // },
  deleted: false
})

var transactionsData = []

function getDateIndex(date) {
  for (let i = 0; i < transactionsData.length; i++) {
    if (transactionsData[i].date == date)
      return i
  }
  return -1
}

function getBranchIndex(dateIndex, branch) {
  for (let i = 0; i < transactionsData[dateIndex].transactions.length; i++) {
    if (transactionsData[dateIndex].transactions[i].branch == branch)
      return i
  }
  return -1
}

transactions.forEach(function (transaction) {
  var driver = db.drivers.findOne({ _id: transaction.driverModelId }) // branch
  var date = new Date(transaction.transactionDate)
  date.setHours(date.getHours() + 3)
  date = date.toISOString().slice(0, 10) // date
  var dateIndex = getDateIndex(date)

  if (dateIndex == -1) {
    dateIndex = transactionsData.length
    transactionsData.push({
      date: date,
      transactions: []
    })
  }

  var branchIndex = getBranchIndex(dateIndex, driver.branch)

  if (branchIndex == -1) {
    branchIndex = transactionsData[dateIndex].transactions.length
    transactionsData[dateIndex].transactions.push({
      branch: driver.branch,
      amount: 0
    })
  }

  transactionsData[dateIndex].transactions[branchIndex].amount += parseFloat(transaction.amount)
})

//print(transactionsData)

transactionsData.forEach(function (dateItem) {
  dateItem.transactions.forEach(function (branchItem) {
    db.receivedcashes.insert({
      date: new Date(dateItem.date),
      branch: branchItem.branch,
      amount: -parseFloat(branchItem.amount).toFixed(2)
    })
  })
})

