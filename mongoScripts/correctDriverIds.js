var captainId = 148
db.drivers.remove({ deleted: true })
var drivers = db.drivers.find({ captainId: { $gt: captainId } })

drivers.forEach(function (driver) {
  captainId++
  db.drivers.update({ _id: driver._id }, { $set: { captainId: parseInt(captainId) } })
})

db.identitycounters.update({ model: 'driver' }, { $set: { count: captainId } })