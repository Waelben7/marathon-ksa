let host = 'https://marathonksa.com/'
let drivers = db.drivers.find({})

drivers.forEach(function (driver) {

  let picture
  let identityIqama
  let drivingLicense

  if (driver.picture)
    picture = host + driver.picture.split('/').pop()

  if (driver.identityIqama)
    identityIqama = host + driver.identityIqama.split('/').pop()

  if (driver.drivingLicense)
    drivingLicense = host + driver.drivingLicense.split('/').pop()

  db.drivers.update({ _id: driver._id }, { $set: { picture, identityIqama, drivingLicense } })
})