var transactions = db.transactions.find({ deleted: true })

transactions.forEach(function (transaction) {
  var driver = db.drivers.findOne({ _id: transaction.driverModelId })
  transaction.driverCarriageId = driver.carriageId

  print(transaction)
})
