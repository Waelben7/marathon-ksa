var drivers = db.drivers.find({captainId: 276})
var n = 0

drivers.forEach(function (driver) {
  var transactions = db.getCollection('transactions').find({ driverModelId: driver._id, deleted: false })
  var sum = transactions.toArray().reduce((s, item) => s + item.balance, 0)
  
  sum = sum.toFixed(2)
  driver.balance = driver.balance.toFixed(2)

  if (sum != driver.balance) {
    n++
    print(driver.captainId, driver.balance, sum, (driver.balance - sum).toFixed(2))
  }
})

print('Total is : ', n)