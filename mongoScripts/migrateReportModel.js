var reports = db.reports.find()

reports.forEach(function (report) {
  var order = db.transactions.findOne({ orderId: report.orderId })
  if (order)
    db.reports.update({ _id: report._id }, { $set: { order: order._id } })
})
