function hideAll() {
	$('#missingFields').hide()
	$('#serverError').hide()
	$('#success').hide()
}

function register() {

	hideAll()

	const params = {
		quantity: document.getElementById('quantity').value,
		fullName: document.getElementById('fullName').value,
		phone: document.getElementById('phone').value,
		address1: document.getElementById('address1').value,
		address2: document.getElementById('address2').value,
		remarks: document.getElementById('remarks').value
	};

	console.log(params)

	if (!(params.fullName && params.phone && params.address1 && params.quantity)) {
		$('#missingFields').show()
	} else {

		let url = window.location.origin

		let fetchData = {
			method: 'POST',
			body: JSON.stringify(params),
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
			}
		}

		fetch(url + '/camOrder', fetchData).then(response => {
			if (response.status === 200) {
				$('#success').show()
			} else {
				$('#serverError').show()
			}
		});
	}
}

hideAll()