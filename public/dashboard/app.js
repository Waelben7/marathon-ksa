var app = angular.module("adminApp", ['ngRoute', 'ngDialog', 'ngFileUpload', 'ui.grid']);

app.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      redirectTo: '/drivers'
    })
    .when('/drivers/:registration?', {
      templateUrl: 'components/driver/drivers.html',
      controller: 'driverController'
    })
    .when('/importTransaction', {
      templateUrl: 'components/importTransaction/importTransaction.html',
      controller: 'importTransactionController'
    })
    .when('/driverTransactions/:driverId', {
      templateUrl: 'components/driverTransaction/driverTransaction.html',
      controller: 'driverTransactionController'
    })
    .when('/driverDocuments/:driverId', {
      templateUrl: 'components/driverDocument/driverDocument.html',
      controller: 'driverDocumentController'
    })
    .when('/profile', {
      templateUrl: 'components/profile/profile.html',
      controller: 'profileController'
    })
    .when('/carriagePayment', {
      templateUrl: 'components/carriagePayment/carriagePayment.html',
      controller: 'carriagePaymentController'
    })
    .when('/pendingTransactions', {
      templateUrl: 'components/pendingTransactions/pendingTransactions.html',
      controller: 'pendingTransactionsController'
    })
    .when('/receivedCash', {
      templateUrl: 'components/receivedCash/receivedCash.html',
      controller: 'receivedCashController'
    })
    .when('/receivedCashDetails/:id', {
      templateUrl: 'components/receivedCashDetails/receivedCashDetails.html',
      controller: 'receivedCashDetailsController'
    })
    .when('/report', {
      templateUrl: 'components/report/reports.html',
      controller: 'reportController'
    })
    .when('/camerasOrder', {
      templateUrl: 'components/camerasOrder/camerasOrders.html',
      controller: 'camerasOrderController'
    })
    .when('/remainingCash', {
      templateUrl: 'components/remainingCash/remainingCash.html',
      controller: 'remainingCashController'
    })
    .when('/cameraOrderInvoice', {
      templateUrl: 'components/cameraOrderInvoice/cameraOrderInvoice.html',
      controller: 'cameraOrderInvoiceController'
    })
    .when('/messages/:captainId', {
      templateUrl: 'components/messages/messages.html',
      controller: 'messagesController'
    })
})


app.controller('adminController', function ($scope, $rootScope, $http, $window) {

  var url = window.location.origin

  $http.get(url + '/user/current').then(function (response) {
    $rootScope.userName = response.data.name
    $rootScope.superAdmin = $scope.userName == 'Admin' || $scope.userName == 'Admin Wael' ? true : false
    $rootScope.abujamel = $scope.userName == 'Abojamal' ? true : false
    $rootScope.samy = $scope.userName == 'Samy' ? true : false
    $rootScope.camerasOrderUser = $scope.userName == 'Zain' || $scope.userName == 'Hanan' ? true : false
    $rootScope.samy = $scope.userName == 'Samy' ? true : false
    $rootScope.hazem = $scope.userName == 'Hazem' ? true : false

    if ($rootScope.onlyCam) {
      window.location.href = "#/camerasOrder";
    }

  }).catch(function (err) {
    console.log(err)
  })

  $rootScope.logout = function () {
    $http.put(url + "/signOut").then(function (response) {
      if (response.status == 204) {
        $window.location.href = '/dashboard/login.html'
      } else {
        console.log(response)
      }
    }).catch(function (err) {
      console.log(err)
    })
  }

})
