app.controller('receivedCashDetailsController', function ($scope, $rootScope, $http, $routeParams, ngDialog) {

  var url = window.location.origin

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'createdAt', displayName: 'Transaction Date', width: '15%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.transactionDate | date:"yyyy-MM-dd | h:mm a"}}</div>' },
      { field: 'paymentMethod', width: '13%', displayName: 'Admin Name' },
      { field: 'amount', width: '7%', cellTemplate: '<div class="ui-grid-cell-contents">{{-row.entity.amount.toFixed(2)}}</div>' },
      { field: 'driverModelId.carriageId', width: '9%', displayName: 'Carriage Id' },
      { field: 'driverModelId.firstName', displayName: 'Driver Name', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.driverModelId.firstName + " " + row.entity.driverModelId.lastName}}</div>' },
      { field: 'driverModelId.phoneNumber', width: '9%', displayName: 'Phone' },
      { field: 'driverModelId.branch', displayName: 'Region', width: '10%' },
    ]
  }

  $http.get(url + '/transactions/receivedCash/' + $routeParams.id).then(function (response) {

    $rootScope.transactions = response.data.transactions

    $scope.gridOptions.data = $rootScope.transactions

  }).catch(function (err) {
    console.log(err);
  })

})
