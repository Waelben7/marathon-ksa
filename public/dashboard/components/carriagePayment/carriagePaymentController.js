app.controller('carriagePaymentController', function ($scope, $rootScope, $http, ngDialog, Upload) {

  var url = window.location.origin
  var fileRemoteName

  $scope.importFile = function () {

    if (fileRemoteName) {
      $scope.loading = true

      $http.post(url + '/carriagePayments/importFile', { fileName: fileRemoteName }).then(function (response) {

        $scope.loading = false
        $scope.message = 'Successful import'
        $scope.fileLocalName = null
        fileRemoteName = null
        $scope.loading = false

        response.data.date = new Date(response.data.date)
        $rootScope.carriagePayments.push(response.data)

        if (response.data.type == 'Affected')
          $rootScope.totalAffected += response.data.amount
        else
          $rootScope.totalDemanded = response.data.amount

      }).catch(function (err) {
        console.log(err)
        $scope.message = 'Something wrong with this file'
        $scope.loading = false
      })
    } else {
      $scope.message = 'Please select file first'
    }
  }

  $scope.verifyFile = function () {

    if (fileRemoteName) {
      $scope.loading = true

      $http.post(url + '/carriagePayments/verifyFile', { fileName: fileRemoteName }).then(function (response) {

        if (response.data.correctFile)
          $scope.message = 'Correct file to import'
        else
          $scope.message = 'Something wrong with this file'

        $scope.loading = false

      }).catch(function (err) {
        console.log(err)
        $scope.message = err
        $scope.loading = false
      })
    } else {
      $scope.message = 'Please select file first'
    }
  }

  $scope.upload = function (file) {
    $scope.inUpload = true
    $scope.message = null
    $scope.fileLocalName = file.name

    Upload.upload({
      url: url + '/upload',
      arrayKey: '',
      data: { file: file }

    }).then(function (resp) {

      fileRemoteName = resp.data.fileUrls[0].split('/').pop()
      $scope.inUpload = false

    }, function (err) {
      console.log('Error status: ' + err.status);
    }, function (evt) {
      $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      console.log($scope.progressPercentage)
    })
  }

  $rootScope.carriagePayments = null
  $rootScope.totalDemanded = null
  $rootScope.totalAffected = null

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'receiptNo', displayName: 'Receipt No.' },
      { field: 'date', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.date | date:"yyyy-MM-dd" }}</div>' },
      { field: 'type', displayName: 'Payment type' },
      { field: 'amount', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.amount.toFixed(2)}}</div>' },
      { field: 'comments' },
      {
        field: 'comments', displayName: 'Actions', cellTemplate: '<div>\
              <button ng-if="grid.appScope.superAdmin" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.editPayment(row.entity._id)">\
                <i class="material-icons">mode_edit</i>\
              </button>\
              <button ng-if="grid.appScope.superAdmin" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-danger" ng-click="grid.appScope.deletePayment(row.entity._id)">\
                <i class="material-icons">delete</i>\
              </button>\
          </div> ' }
    ]
  }

  $http.get(url + '/carriagePayments').then(function (response) {
    $rootScope.carriagePayments = response.data.carriagePayments.map(function (item) {
      item.date = new Date(item.date)
      return item
    })

    $rootScope.totalDemanded = response.data.totalDemanded
    $rootScope.totalAffected = response.data.totalAffected
    $scope.gridOptions.data = $rootScope.carriagePayments

  }).catch(function (err) {
    console.log(err);
  })

  function getElementIndex(_id) {
    for (var i = 0; i < $rootScope.carriagePayments.length; i++) {
      if ($rootScope.carriagePayments[i]._id == _id)
        return i
    }
    return -1
  }

  $scope.newPayment = function () {

    ngDialog.open({
      template: 'components/carriagePayment/payment.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Add new payment"
        $rootScope.paymentObject = {}

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.post(url + '/carriagePayments', $rootScope.paymentObject).then(function (response) {

            response.data.date = new Date(response.data.date)
            $rootScope.carriagePayments.push(response.data)

            if (response.data.type == 'Affected')
              $rootScope.totalAffected += response.data.amount
            else
              $rootScope.totalDemanded = response.data.amount

            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })

        }
      }]
    })
  }

  $scope.deletePayment = function (_id) {
    ngDialog.open({
      template: 'assets/delete.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.delete(url + '/carriagePayments/' + _id).then(function (response) {
            var index = getElementIndex(_id)

            if ($rootScope.carriagePayments[index].type == 'Affected')
              $rootScope.totalAffected -= $rootScope.carriagePayments[index].amount
            else
              $rootScope.totalDemanded -= $rootScope.carriagePayments[index].amount

            $rootScope.carriagePayments.splice(index, 1)

          }).catch(function (err) {
            console.log(err)
          }).finally(function () {
            $scope.loading = false
            $scope.closeThisDialog()
          })
        }
      }]
    })
  }

  $scope.editPayment = function (_id) {

    $rootScope.paymentObject = {}

    ngDialog.open({
      template: 'components/carriagePayment/payment.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Edit Payment"
        var index = getElementIndex(_id)
        $rootScope.paymentObject = angular.copy($rootScope.carriagePayments[index])

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.put(url + '/carriagePayments/' + $rootScope.paymentObject._id, $rootScope.paymentObject).then(function (response) {

            if ($rootScope.carriagePayments[index].type == 'Affected')
              $rootScope.totalAffected -= $rootScope.carriagePayments[index].amount
            else
              $rootScope.totalDemanded -= $rootScope.carriagePayments[index].amount


            if ($rootScope.paymentObject.type == 'Affected')
              $rootScope.totalAffected += $rootScope.paymentObject.amount
            else
              $rootScope.totalDemanded += $rootScope.paymentObject.amount

              
            $rootScope.carriagePayments[index] = angular.copy($rootScope.paymentObject)

            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })
        }
      }]
    })
  }
})
