app.controller('remainingCashController', function ($scope, $http) {

  var url = window.location.origin

  $http.post(url + '/balancePerRegion').then(function (response) {

    $scope.remainingCash = response.data
    console.log(response.data)

  }).catch(function (err) {
    console.log(err);
  })

})
