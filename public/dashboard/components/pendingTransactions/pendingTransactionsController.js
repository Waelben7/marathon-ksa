app.controller('pendingTransactionsController', function ($scope, $rootScope, $http, $routeParams, ngDialog, Upload) {

  var url = window.location.origin

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'transactionDate', width: '13%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.transactionDate | date:"yyyy-MM-dd" }}</div>' },
      { field: 'orderId', width: '7%' },
      { field: 'amount', width: '7%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.amount.toFixed(2)}}</div>' },
      { field: 'transactionType', width: '13%' },
      { field: 'paymentMethod', width: '13%' },
      { field: 'driverModelId.carriageId', width: '9%', displayName: 'Carriage Id' },
      { field: 'driverModelId.firstName', displayName: 'Driver Name', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.driverModelId.firstName + " " + row.entity.driverModelId.lastName}}</div>' },
      { field: 'driverModelId.phoneNumber', width: '9%', displayName: 'Phone' },
      { field: 'driverModelId.branch', displayName: 'Region', width: '10%' },
    ]
  }

  $http.get(url + '/transactions/byStatus/pending').then(function (response) {

    $rootScope.transactions = response.data.transactions.map(function (transaction) {
      transaction.transactionDate = new Date(transaction.transactionDate)
      return transaction
    })

    $scope.gridOptions.data = $rootScope.transactions

  }).catch(function (err) {
    console.log(err);
  })

  $scope.updateStatus = function () {
    $http.put(url + '/transaction/' + $scope.orderID, { status: $scope.status }).then(function (response) {

      $scope.gridOptions.data = $rootScope.transactions = $rootScope.transactions.filter(item => item.orderId != $scope.orderID)
      $scope.orderID = null
      
    }).catch(function (err) {
      console.log(err);
    })
  }

})
