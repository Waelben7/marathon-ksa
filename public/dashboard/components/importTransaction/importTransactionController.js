app.controller('importTransactionController', function ($scope, $rootScope, $http, ngDialog, Upload) {

  var url = window.location.origin
  var fileRemoteName

  $scope.importFile = function () {

    if (fileRemoteName) {
      $scope.loading = true

      $http.post(url + '/transaction/importFile', { fileName: fileRemoteName }).then(function (response) {

        $scope.fileLocalName = null
        fileRemoteName = null
        $scope.loading = false

        if (response.data.correctFile)
          $scope.message = 'Successful import'
        else {
          if (response.data.error == 'NotIsSystemDrivers') {
            $scope.message = 'Not in system carriage Ids : '
            response.data.notFoundDrivers.forEach(carriageId => {
              $scope.message += (carriageId + ', ')
            })
            $scope.message = $scope.message.slice(0, -2)

          } else {
            $scope.message = response.data.error
          }
        }

      }).catch(function (err) {
        console.log(err)
        $scope.message = 'Something wrong with this file. ' + err.data.error
        $scope.fileLocalName = null
        fileRemoteName = null
        $scope.loading = false
      })

    } else {
      $scope.message = 'Please select file first'
    }
  }

  $scope.verifyFile = function () {

    if (fileRemoteName) {
      $scope.loading = true

      $http.post(url + '/transaction/verifyFile', { fileName: fileRemoteName }).then(function (response) {

        if (response.data.correctFile)
          $scope.message = 'Correct file to import'
        else {
          if (response.data.error == 'NotIsSystemDrivers') {
            $scope.message = 'Not in system carriage Ids : '
            response.data.notFoundDrivers.forEach(carriageId => {
              $scope.message += (carriageId + ', ')
            })
            $scope.message = $scope.message.slice(0, -2)

          } else {
            $scope.message = response.data.error
          }
        }

        $scope.loading = false

      }).catch(function (err) {
        console.log(err)
        $scope.message = err
        $scope.loading = false
      })
    } else {
      $scope.message = 'Please select file first'
    }
  }

  $scope.upload = function (file) {
    $scope.loading = true
    $scope.message = null
    $scope.fileLocalName = file.name

    Upload.upload({
      url: url + '/upload',
      arrayKey: '',
      data: { file: file }

    }).then(function (resp) {

      fileRemoteName = resp.data.fileUrls[0].split('/').pop()
      $scope.loading = false

    }, function (err) {
      console.log('Error status: ' + err.status);
    }, function (evt) {
      $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      console.log($scope.progressPercentage)
    })
  }
})
