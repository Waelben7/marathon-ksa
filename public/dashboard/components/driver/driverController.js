app.controller('driverController', function ($scope, $rootScope, $http, ngDialog, Upload, $routeParams) {

  var url = window.location.origin

  $rootScope.drivers = null
  $rootScope.driverObject = {}
  $rootScope.branches = ['Riyadh', 'Eastern Region', 'Jeddah', 'Alhassa']
  $rootScope.countryNames = [
    "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"
  ];

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    rowTemplate: '<div ng-class="{red : row.entity.status!=\'Working\'}" ng-repeat="col in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell></div>',
    columnDefs: [
      { field: 'captainId', displayName: 'ID', width: '4%' },
      { field: 'carriageId', width: '6%' },
      { field: 'branch', displayName: 'Region', width: '7%' },
      { field: 'firstName', displayName: 'Name', width: '13%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.firstName + " " + row.entity.lastName}}</div>' },
      { field: 'phoneNumber', displayName: 'Phone' },
      { field: 'nationality' },
      {
        field: 'picture', displayName: 'Photo', width: '5%', cellTemplate: '<div style="margin-left:2px; margin-top:2px">\
          <button ng-disabled="!row.entity.picture" type="button" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info"\
          ng-click="grid.appScope.openInNewTab(row.entity.picture)">\
              <i class="material-icons">search</i>\
          </button>\
        </div> ' },
      { field: 'balance', width: '6%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.balance.toFixed(2)}}</div>' },
      { field: 'createdAt', displayName: 'Reg.Date', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.createdAt | date:"yyyy-MM-dd" }}</div>' },
      { field: 'status', displayName: 'Status', width: '6%' },
      { field: 'comments' },
      {
        field: 'comments', displayName: 'Actions', width: '16%', cellTemplate: '<div>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.driverPayment(row.entity.captainId)">\
                <i class="material-icons">attach_money</i>\
              </button>\
              <button ng-disabled="!row.entity.fcm" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openDriverMessages(row.entity.captainId)">\
                <i class="material-icons">message</i>\
              </button>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openDriverTransactions(row.entity.captainId)">\
                <i class="material-icons">swap_horiz</i>\
              </button>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openDriverDocuments(row.entity.captainId)">\
                <i class="material-icons">print</i>\
              </button>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.editDriver(row.entity.captainId)">\
                <i class="material-icons">mode_edit</i>\
              </button>\
              <button ng-if="grid.appScope.superAdmin" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-danger" ng-click="grid.appScope.deleteDriver(row.entity.captainId)">\
                <i class="material-icons">delete</i>\
              </button>\
          </div> '
      }
    ]
  }

  const registrationPram = $routeParams.registration == undefined ? '' : $routeParams.registration

  $http.get(url + '/drivers/' + registrationPram).then(function (response) {
    $rootScope.drivers = response.data.drivers.map(function (driver) {
      driver.dateOfBirth = new Date(driver.dateOfBirth)
      driver.registrationDate = new Date(driver.registrationDate)
      return driver
    })

    $scope.gridOptions.data = $rootScope.drivers

    console.log($rootScope.drivers);
    

  }).catch(function (err) {
    console.log(err);
  })

  function getElementIndex(captainId) {
    for (var i = 0; i < $rootScope.drivers.length; i++) {
      if ($rootScope.drivers[i].captainId == captainId)
        return i
    }
    return -1
  }

  $scope.addDriver = function () {

    $rootScope.driverObject = {}

    ngDialog.open({
      template: 'components/driver/driver.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Add new Driver"

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.post(url + '/driver', $rootScope.driverObject).then(function (response) {

            response.data.driver.dateOfBirth = new Date(response.data.driver.dateOfBirth)
            response.data.driver.identityDate = new Date(response.data.driver.identityDate)
            response.data.driver.custodyDate = new Date(response.data.driver.custodyDate)
            response.data.driver.registrationDate = new Date(response.data.driver.registrationDate)

            $rootScope.drivers.push(response.data.driver)

            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            $scope.message = err.data.error
            console.log(err)
          })

        }
      }]
    })
  }

  $scope.sendMessageToAll = function () {

    ngDialog.open({
      template: 'components/driver/message.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Send message to all drivers"

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true

          $http.post(url + '/driver/messageToAll', $scope.message).then(function (response) {

            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {

            console.log(err)
            $scope.errorMessage = err.data.error
          })
        }
      }]
    })
  }

  $scope.editDriver = function (captainId) {

    $rootScope.driverObject = {}

    ngDialog.open({
      template: 'components/driver/driver.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(captainId)
        $scope.modelTitle = "Edit Driver"
        $rootScope.driverObject = angular.copy($rootScope.drivers[index])

        console.log($rootScope.driverObject)

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.put(url + '/driver/' + $rootScope.driverObject._id, $rootScope.driverObject).then(function (response) {
            $rootScope.drivers[index] = angular.copy($rootScope.driverObject)
            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })

        }
      }]
    })
  }

  $scope.deleteDriver = function (captainId) {
    ngDialog.open({
      template: 'assets/delete.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(captainId)

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.delete(url + '/driver/' + $rootScope.drivers[index]._id).then(function (response) {
            $rootScope.drivers.splice(index, 1)
          }).catch(function (err) {
            console.log(err)
          }).finally(function () {
            $scope.loading = false
            $scope.closeThisDialog()
          })
        }
      }]
    })
  }

  $scope.driverPayment = function (captainId) {

    ngDialog.open({
      template: 'components/driver/payment.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(captainId)
        $scope.modelTitle = "Driver payment"
        $scope.driver = $rootScope.drivers[index]

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          var body = {
            carriageId: $scope.driver.carriageId,
            amount: $scope.amount
          }

          $http.post(url + '/transaction/payment', body).then(function (response) {

            $rootScope.drivers[index].balance -= $scope.amount

            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)

            if (err.data.code == 113) {
              $scope.errorMessage = err.data.error
            }
          })

        }
      }]
    })
  }

  $rootScope.upload = function (file, distination) {
    $scope.inUpload = true

    Upload.upload({
      url: url + '/upload',
      arrayKey: '',
      data: { file: file }

    }).then(function (resp) {

      const fileUrl = resp.data.fileUrls[0]

      switch (distination) {
        case 'picture':
          $rootScope.driverObject.picture = fileUrl
          break;
        case 'identityIqama':
          $rootScope.driverObject.identityIqama = fileUrl
          break;
        case 'drivingLicense':
          $rootScope.driverObject.drivingLicense = fileUrl
          break;

        default:
          break;
      }

      $scope.inUpload = false

    }, function (err) {
      console.log('Error status: ' + err.status);
    }, function (evt) {
      $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      console.log($scope.progressPercentage)
    })
  }

  $rootScope.openInNewTab = function (url) {
    window.open(url, '_blank')
  }

  $scope.openDriverTransactions = function (captainId) {
    window.open(url + '/dashboard/#/driverTransactions/' + captainId, '_blank')
  }

  $scope.openDriverDocuments = function (captainId) {
    window.open(url + '/dashboard/#/driverDocuments/' + captainId, '_blank')
  }

  $scope.openDriverMessages = function (captainId) {
    window.open(url + '/dashboard/#/messages/' + captainId, '_blank')
  }

})
