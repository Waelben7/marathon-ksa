app.controller('reportController', function ($scope, $rootScope, $http, ngDialog) {

  var url = window.location.origin
  $scope.filterStatus = 'pending'
  $scope.filterRegion = 'All Regions'

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'number', width: '4%', displayName: 'ID' },
      { field: 'createdAt', displayName: 'Report date', width: '13%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.createdAt | date:"yyyy-MM-dd | h:mm a" }}</div>' },
      { field: 'orderId', width: '7%', displayName: 'Order Id' },
      {
        field: 'order.transactionDate', displayName: 'Order date', width: '13%',
        cellTemplate: '<div ng-if="row.entity.order" class="ui-grid-cell-contents">{{row.entity.order.transactionDate | date:"yyyy-MM-dd | h:mm a" }}</div>\
                      <div ng-if="!row.entity.order" class="ui-grid-cell-contents">Not In System</div>'
      },
      {
        field: 'order.status', width: '9%', displayName: 'Order Status',
        cellTemplate: '<div ng-if="row.entity.order" class="ui-grid-cell-contents">{{row.entity.order.status}}</div>\
                      <div ng-if="!row.entity.order" class="ui-grid-cell-contents">Not In System</div>'
      },
      {
        field: 'order.amount', width: '9%', displayName: 'Order Amount',
        cellTemplate: '<div ng-if="row.entity.order" class="ui-grid-cell-contents">{{row.entity.order.amount}}</div>\
                      <div ng-if="!row.entity.order" class="ui-grid-cell-contents">Not In System</div>'
      },
      { field: 'message' },
      { field: 'driver.carriageId', width: '8%', displayName: 'Carriage Id' },
      { field: 'driver.firstName', width: '15%', displayName: 'Driver Name', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.driver.firstName + " " + row.entity.driver.lastName}}</div>' },
      { field: 'driver.branch', displayName: 'Region', width: '10%' },
      {
        field: 'createdAt', displayName: 'Actions', width: '6%', cellTemplate: '<div>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.takeAction(row.entity._id)">\
                <i class="material-icons">ballot</i>\
              </button>\
          </div> '
      }
    ]
  }

  function reloadData() {
    if ($scope.filterRegion != 'All Regions')
      $scope.gridOptions.data = $rootScope.reports.filter(report =>
        report.status == $scope.filterStatus && report.driver.branch == $scope.filterRegion
      )
    else
      $scope.gridOptions.data = $rootScope.reports.filter(report => report.status == $scope.filterStatus)

    $scope.reportsCount = $scope.gridOptions.data.length
  }

  $http.get(url + '/reports').then(function (response) {

    $rootScope.reports = response.data.reports
    reloadData()
  }).catch(function (err) {
    console.log(err);
  })

  function getElementIndex(_id) {
    for (var i = 0; i < $rootScope.reports.length; i++) {
      if ($rootScope.reports[i]._id == _id)
        return i
    }
    return -1
  }

  $scope.takeAction = function (_id) {

    ngDialog.open({
      template: 'components/report/report.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(_id)

        $scope.report = $rootScope.reports[index]
        $scope.modelObject = {
          status: $scope.report.status,
          response: $scope.report.response,
          disable: $scope.report.response ? true : false
        }

        if ($scope.report.order)
          $scope.modelObject.newOrderStatus = $scope.report.order.status

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.put(url + '/transaction/' + $scope.report.orderId, { status: $scope.modelObject.newOrderStatus }).then(function (response) {

            if ($scope.modelObject.response && $scope.modelObject.response != "") {

              $scope.modelObject.response += ('\n\n\n' + $rootScope.userName
                + ' | ' + new Date().toISOString().slice(0, 16).replace('T', ' '))
            }

            $http.put(url + '/report/' + $scope.report._id, {
              status: $scope.modelObject.status,
              response: $scope.modelObject.response

            }).then(function (response) {

              $scope.report.order.status = $scope.modelObject.newOrderStatus
              $scope.report.status = $scope.modelObject.status
              $scope.report.response = $scope.modelObject.response
              $scope.loading = false

              reloadData()
              $scope.closeThisDialog()
            }).catch(function (err) {
              $scope.message = err.data.error
              console.log(err)
            })

          }).catch(function (err) {
            $scope.message = err.data.error
            console.log(err)
          })
        }
      }]
    })
  }

  $scope.filterUpdate = function () {
    reloadData()
  }

})
