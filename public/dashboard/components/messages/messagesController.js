app.controller('messagesController', function ($scope, $rootScope, $http, $routeParams, ngDialog) {

  var url = window.location.origin

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'date', width: '14%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.date | date:"yyyy-MM-dd | h:mm a" }}</div>' },
      { field: 'title', width: '30%' },
      { field: 'body' },
      {
        field: 'referance', displayName: 'Actions', width: '6%', cellTemplate: '<div>\
              <button ng-if="grid.appScope.superAdmin || grid.appScope.samy" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-danger" ng-click="grid.appScope.deleteMessage(row.entity._id)">\
                <i class="material-icons">delete</i>\
              </button>\
          </div> ' }
    ]
  }

  $http.get(url + '/driver/' + $routeParams.captainId).then(function (response) {
    $rootScope.driver = response.data.driver
  }).catch(function (err) {
    console.log(err);
  })

  $http.get(url + '/driver/' + $routeParams.captainId + '/messages').then(function (response) {
    $rootScope.messages = response.data
    $scope.gridOptions.data = $rootScope.messages
  }).catch(function (err) {
    console.log(err);
  })

  function getElementIndex(id) {
    for (var i = 0; i < $rootScope.messages.length; i++) {
      if ($rootScope.messages[i]._id == id)
        return i
    }
    return -1
  }

  $scope.deleteMessage = function (id) {

    ngDialog.open({
      template: 'assets/delete.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true
          $http.delete(url + '/driver/' + $rootScope.driver._id + '/message/' + id).then(function () {

            var index = getElementIndex(id)
            $rootScope.messages.splice(index, 1)

          }).catch(function (err) {
            console.log(err);
          }).finally(function () {
            $scope.loading = false
            $scope.closeThisDialog()
          })
        }
      }]
    })

  }

  $scope.sendMessage = function () {

    ngDialog.open({
      template: 'components/messages/message.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Send message to " + $rootScope.driver.firstName + ' ' + $rootScope.driver.lastName

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true

          $http.post(url + '/driver/' + $rootScope.driver._id + '/message', $scope.message).then(function (response) {
            $rootScope.messages.push(response.data)
            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {

            console.log(err)
            $scope.errorMessage = err.data.error
          })
        }
      }]
    })
  }

})
