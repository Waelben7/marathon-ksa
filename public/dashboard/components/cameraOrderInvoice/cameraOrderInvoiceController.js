app.controller('cameraOrderInvoiceController', function ($scope, $rootScope, $http, ngDialog) {

  let current_datetime = new Date()
  let today = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()

  $scope.invoice = {
    title: 'Quotation for HD Cameras',
    company: {
      from: 'Marathon Security Camera company',
      mobile: '0552105221',
      date: today,
      email: 'sales@marathonksa.com'
    },
    customer: {
      date: today,
    },
    terms: [
      'All prices in Saudi Arabia.',
      'Validity: Two weeks from Quotation date.',
      'Payment: Advanced 80% , Upon Completion 20%.',
      'Delivery: 2-3 working Days from P.O. and down Payment.',
      'Warranty: Two years against any manufacturer fault.'
    ],
    items: [{}]
  }

  $scope.addNewTerm = function () {
    $scope.invoice.terms.push('')
  }

  $scope.deleteTerm = function (index) {
    $scope.invoice.terms.splice(index, 1)
  }

  $scope.addNewItem = function () {
    $scope.invoice.items.push({})
  }

  $scope.deleteItem = function (index) {
    $scope.invoice.items.splice(index, 1)
  }

  $scope.printInvoice = function () {
    var myWindow = window.open('', '', 'width=794');
    myWindow.document.write(getHTMLInvoice());

    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    setTimeout(() => myWindow.close(), 1000)
  }

  function getHTMLInvoice() {

    var invoice = $scope.invoice
    var invoiceTotalPrice = 0

    var html = `
        <head>
      <meta charset="utf-8">

      <style>
        body {
          margin: 0;
          background-color: bisque
        }

        .page {
          max-width: 210mm;
          height: 296mm;
          background-image: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)), url("./assets/img/background.png");
          background-repeat: no-repeat;
          background-position: center center;
        }

        table {
          border-collapse: collapse;
        }

        table,
        th,
        td {
          border: 1px solid black;
        }

        .center {
          margin: 0 auto
        }
      </style>

    </head>

    <body>
      <div class="page">
        <br>

        <div>
          <img src="./assets/img/background.png" style="max-width:100px; margin-left: 50px; margin-top: 20px">
          <h4 style="float:right; margin-right: 50px; margin-top: 30px">Ref : 
          `+ invoice.reference + `
          </h4>
        </div>

        <h1 style="text-align:center;margin-top: 80px">
        `+ invoice.title + `
        </h1>

        <table class="center" style="width:90%; margin-top: 90px">
          <tr>
            <td style="width:50%">
              <strong>&nbsp From &nbsp&nbsp&nbsp : &nbsp</strong>
              `+ invoice.company.from + `
            </td>
            <td style="width:50%">
              <strong>&nbsp To &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
              `+ invoice.customer.to + `
            </td>
          </tr>
          <tr>
            <td>
              <strong>&nbsp Mobile &nbsp : &nbsp</strong>
              `+ invoice.company.mobile + `
            </td>
            <td>
              <strong>&nbsp Mobile &nbsp : &nbsp</strong>
              `+ invoice.customer.mobile + `
            </td>
          </tr>
          <tr>
            <td>
              <strong>&nbsp Date &nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
              `+ invoice.company.date + `
            </td>
            <td>
              <strong>&nbsp Date &nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
              `+ invoice.customer.date + `
            </td>
          </tr>
          <tr>
            <td>
              <strong>&nbsp Email &nbsp&nbsp : &nbsp</strong>
              `+ invoice.company.email + `
            </td>
            <td>
              <strong>&nbsp Email &nbsp&nbsp : &nbsp</strong>
              `+ invoice.customer.email + `
            </td>
          </tr>
        </table>

        <table class="center" style="width:90%; margin-top: 90px; text-align:center">
          <tr style="background-color:rgba(0, 153, 255, 0.678); color: white">
            <th style="width:50%">Item description</th>
            <th>Quantity</th>
            <th>Unit Price / SR</th>
            <th>Total Price / SR</th>
          </tr>
      `

    for (item of invoice.items) {
      itemTotalPrice = item.quantity * item.unitPrice
      invoiceTotalPrice += itemTotalPrice
      html +=

        `
        <tr>
          <td>
          `+ item.description + `
          </td>
          <td>
          `+ item.quantity + `
          </td>
          <td>
          `+ item.unitPrice + `
          </td>
          <td>
          `+ itemTotalPrice + `
          </td>
        </tr>
        `
    }

    html +=
      `
      <tr>
      <td colspan="3">Total Price / SR</td>
      <td>
      `+ invoiceTotalPrice + `
      </td>
      </tr>
      </table>

        <div style="margin-left:100px; margin-top:50px">
          <h3>Terms and conditions :</h3>
          <ul>
      `

    for (item of invoice.terms) {
      html += ("<li>" + item + "</li>\n")
    }

    html +=

      `
      </ul>
      </div>

      <div style="width:210mm; position: absolute; top: 280mm">
      <div class="center" style="width:90%; text-align: center">
        <hr>
        <strong>Marathon Security Camera company</strong>&nbsp&nbsp Email : sales@marathonksa.com &nbsp&nbsp Tel : 0552105221
      </div>
      </div>

      </div>
      </body>
    `

    return html
  }

})
