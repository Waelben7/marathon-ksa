app.controller('driverTransactionController', function ($scope, $rootScope, $http, $routeParams, ngDialog, Upload) {

  var url = window.location.origin
  $scope.title = 'Driver transactions'
  $scope.selectedAction = 'Change status'
  $scope.newCarriageId = null

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'transactionDate', width: '14%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.transactionDate | date:"yyyy-MM-dd | h:mm a" }}</div>' },
      { field: 'orderId' },
      { field: 'status' },
      { field: 'amount', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.amount.toFixed(2)}}</div>' },
      { field: 'transactionType' },
      { field: 'paymentMethod' },
      { field: 'balance', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.balance.toFixed(2)}}</div>' },
      { field: 'referance', width: '20%' },
      {
        field: 'referance', displayName: 'Actions', width: '6%', cellTemplate: '<div>\
              <button ng-if="grid.appScope.superAdmin || grid.appScope.samy || grid.appScope.hazem" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-danger" ng-click="grid.appScope.deleteTransaction(row.entity._id)">\
                <i class="material-icons">delete</i>\
              </button>\
              <button ng-if="row.entity.details" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openDetails(row.entity._id)">\
                <i class="material-icons">assignment</i>\
              </button>\
          </div> ' }
    ]
  }

  $http.get(url + '/driver/' + $routeParams.driverId).then(function (response) {
    $rootScope.driver = response.data.driver
  }).catch(function (err) {
    console.log(err);
  })

  $http.get(url + '/transactions/byDriver/' + $routeParams.driverId).then(function (response) {

    $rootScope.transactions = response.data.transactions.map(function (transaction) {
      transaction.transactionDate = new Date(transaction.transactionDate)
      return transaction
    })

    $scope.gridOptions.data = $rootScope.transactions

  }).catch(function (err) {
    console.log(err);
  })

  $scope.updateStatus = function () {
    $http.put(url + '/transaction/' + $scope.orderID, { status: $scope.status }).then(function (response) {

      $rootScope.driver.balance = response.data.driverBalance

      for (let transaction of $rootScope.transactions) {
        if (transaction.orderId == $scope.orderID) {
          transaction.balance = response.data.transactionBalance
          transaction.status = $scope.status
          break
        }
      }

    }).catch(function (err) {
      console.log(err);
    })
  }

  $scope.transfer = function () {

    $http.put(url + '/transaction/transfer', { orderId: $scope.orderID, carriageId: $scope.newCarriageId }).then(function (response) {
      $rootScope.driver.balance = response.data.driverBalance

      $scope.gridOptions.data = $rootScope.transactions = $rootScope.transactions.filter(item => item.orderId != $scope.orderID)
      $scope.orderID = null
      scope.newCarriageId = null

    }).catch(function (err) {
      console.log(err);
    })
  }

  function getElementIndex(transactionId) {
    for (var i = 0; i < $rootScope.transactions.length; i++) {
      if ($rootScope.transactions[i]._id == transactionId)
        return i
    }
    return -1
  }

  $scope.deleteTransaction = function (transactionId) {

    ngDialog.open({
      template: 'assets/delete.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.delete(url + '/transaction/' + transactionId, {}).then(function (response) {

            var index = getElementIndex(transactionId)
            $rootScope.driver.balance -= $rootScope.transactions[index].balance
            $rootScope.transactions.splice(index, 1)

          }).catch(function (err) {
            console.log(err);
          }).finally(function () {
            $scope.loading = false
            $scope.closeThisDialog()
          })
        }
      }]
    })

  }

  $scope.openDetails = function (transactionId) {
    ngDialog.open({
      template: 'components/driverTransaction/details.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(transactionId)

        $scope.details = $rootScope.transactions[index].details
        
      }]
    })
  }

  $scope.addOrder = function () {

    ngDialog.open({
      template: 'components/driverTransaction/order.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.modelTitle = "Add manual order"

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true
          $scope.modelObject.driverModelId = $rootScope.driver._id

          $http.post(url + '/transaction/manualOrder', $scope.modelObject).then(function (response) {
            $rootScope.transactions.push(response.data)
            $rootScope.driver.balance += response.data.balance
            $scope.loading = false
            $scope.closeThisDialog()
          
          }).catch(function (err) {
            console.log(err)
            $scope.errorMessage = err.data.error
          
          })
        }
      }]
    })
  }
})
