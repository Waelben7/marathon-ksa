app.controller('camerasOrderController', function ($scope, $rootScope, $http, ngDialog) {

  var url = window.location.origin

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'number', displayName: 'ID', width: '4%' },
      { field: 'fullName' },
      { field: 'quantity', width: '7%' },
      { field: 'phone' },
      { field: 'address1', displayName: 'City' },
      { field: 'address2', displayName: 'Street' },
      { field: 'remarks' },
      { field: 'comments', displayName: 'Last Comment', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.comments[row.entity.comments.length - 1].text}}</div>' },
      { field: 'createdAt', displayName: 'Order date', width: '13%', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.createdAt | date:"yyyy-MM-dd h:mm a" }}</div>' },
      {
        field: 'createdAt', displayName: 'Edit', width: '8%', cellTemplate: '<div>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openComments(row.entity._id)">\
                <i class="material-icons">ballot</i>\
              </button>\
              <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openDetails(row.entity._id)">\
              <i class="material-icons">mode_edit</i>\
            </button>\
          </button>\
          </div> '
      }
    ]
  }

  $http.get(url + '/camOrders').then(function (response) {
    $rootScope.camOrders = response.data.camOrders
    $scope.gridOptions.data = $rootScope.camOrders
  }).catch(function (err) {
    console.log(err);
  })

  function getElementIndex(_id) {
    for (var i = 0; i < $rootScope.camOrders.length; i++) {
      if ($rootScope.camOrders[i]._id == _id)
        return i
    }
    return -1
  }

  $scope.openComments = function (_id) {

    ngDialog.open({
      template: 'components/camerasOrder/commentsModel.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(_id)

        $scope.modelObject = $rootScope.camOrders[index]

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.post(url + '/camOrder/' + $scope.modelObject._id + '/comment', {
            text: $scope.modelObject.newComment
          }).then(function (response) {

            $scope.modelObject.comments.push(response.data.comment)
            $scope.modelObject.newComment = ""
            $scope.loading = false

          }).catch(function (err) {
            $scope.message = err.data.error
            console.log(err)
          })
        }
      }]
    })
  }

  $scope.openDetails = function (_id) {

    ngDialog.open({
      template: 'components/camerasOrder/camerasOrder.html',
      className: 'ngdialog-theme-default',
      width: '700px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        var index = getElementIndex(_id)

        $scope.modelObject = angular.copy($rootScope.camOrders[index])

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          loading = true

          $http.put(url + '/camOrder/' + $scope.modelObject._id, $scope.modelObject).then(function (response) {

            $rootScope.camOrders[index] = angular.copy($scope.modelObject)
            $scope.loading = false

            $scope.closeThisDialog()

          }).catch(function (err) {
            $scope.message = err.data.error
            console.log(err)
          })
        }
      }]
    })
  }

  $scope.openInNewTab = function (url) {
    window.open(url, '_blank')
  }

  $scope.print = function (id) {

    var order = $rootScope.camOrders[getElementIndex(id)]
    var myWindow = window.open('', '', 'width=794');
    myWindow.document.write(getHTMLInvoice(order));

    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    setTimeout(() => myWindow.close(), 1000)
  }

  function getHTMLInvoice(order) {

    let unitPrice = 100
    let totalPrice = order.quantity * unitPrice
    let current_datetime = new Date()
    let today = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()

    var html = `
    <head>
  <meta charset="utf-8">

  <style>
    body {
      margin: 0;
      background-color: bisque
    }

    .page {
      max-width: 210mm;
      height: 296mm;
      background-image: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)), url("./assets/img/background.png");
      background-repeat: no-repeat;
      background-position: center center;
    }

    table {
      border-collapse: collapse;
    }

    table,
    th,
    td {
      border: 1px solid black;
    }

    .center {
      margin: 0 auto
    }
  </style>

</head>

<body>
  <div class="page">
    <br>

    <div>
      <img src="./assets/img/background.png" style="max-width:100px; margin-left: 50px; margin-top: 20px">
      <h4 style="float:right; margin-right: 50px; margin-top: 30px">Ref : 
      `+ (order.number + 1000) + `
      </h4>
    </div>

    <h1 style="text-align:center;margin-top: 80px">Quotation for HD Cameras</h1>

    <table class="center" style="width:90%; margin-top: 90px">
      <tr>
        <td style="width:50%">
          <strong>&nbsp From &nbsp&nbsp&nbsp : &nbsp</strong>
          Marathon Security Camera company
        </td>
        <td style="width:50%">
          <strong>&nbsp To &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
          `+ order.fullName + `
        </td>
      </tr>
      <tr>
        <td>
          <strong>&nbsp Mobile &nbsp : &nbsp</strong>
          0552105221
        </td>
        <td>
          <strong>&nbsp Mobile &nbsp : &nbsp</strong>
          `+ order.phone + `
        </td>
      </tr>
      <tr>
        <td>
          <strong>&nbsp Date &nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
          `+ today + `
        </td>
        <td>
          <strong>&nbsp Date &nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp</strong>
          `+ today + `
        </td>
      </tr>
      <tr>
        <td>
          <strong>&nbsp Email &nbsp&nbsp : &nbsp</strong>
          sales@marathonksa.com
        </td>
        <td>
          <strong>&nbsp Email &nbsp&nbsp : &nbsp</strong>
          waelben7@gmail.com
        </td>
      </tr>
    </table>

    <table class="center" style="width:90%; margin-top: 90px; text-align:center">
      <tr>
        <th>Item</th>
        <th style="width:40%">Description</th>
        <th>Quantity</th>
        <th>Unit Price / SR</th>
        <th>Total Price / SR</th>
      </tr>
      <tr>
        <td>
          <img src="./assets/img/cam07.png" style="max-width:100px">
        </td>
        <td>
          HD Camera 5MP Dome Indoor, Fixed 3.6mm Lens - DS-
          2CE56H0T-ITMF
        </td>
        <td>
        `+ order.quantity + `
        </td>
        <td>
        `+ unitPrice + `
        </td>
        <td>
        `+ totalPrice + `
        </td>
      </tr>
      <tr>
        <td colspan="4">Total Price</td>
        <td>
        `+ totalPrice + `
        </td>
      </tr>
      <tr>
        <td colspan="4">Value Added Tax 5%</td>
        <td>
        `+ totalPrice / 100 * 5 + `
        </td>
      </tr>
      <tr>
        <td colspan="4">Net Offer</td>
        <td>
        `+ ((totalPrice / 100 * 5) + totalPrice) + `
        </td>
      </tr>
    </table>

    <div style="margin-left:100px; margin-top:50px">
      <h3>Terms and conditions :</h3>
      <ul>
        <li>All prices in Saudi Arabia.</li>
        <li>Validity: Two weeks from Quotation date</li>
        <li>Payment: Advanced 80% , Upon Completion 20%</li>
        <li>Delivery: 2-3 working Days from P.O. and down Payment</li>
        <li>Warranty: Two years against any manufacturer fault.</li>
      </ul>
    </div>

    <div style="width:210mm; position: absolute; top: 280mm">
      <div class="center" style="width:90%; text-align: center">
        <hr>
        <strong>Marathon Security Camera company</strong>&nbsp&nbsp Email : sales@marathonksa.com &nbsp&nbsp Tel : 0552105221
      </div>
    </div>

  </div>
</body>
    `

    return html

  }

})
