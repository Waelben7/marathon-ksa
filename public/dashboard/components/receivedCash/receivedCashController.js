app.controller('receivedCashController', function ($scope, $rootScope, $http, $routeParams, ngDialog, Upload) {

  var url = window.location.origin

  $scope.gridOptions = {
    enableFiltering: true,
    autoResize: true,
    onRegisterApi: function (gridApi) {
      this.gridApi = gridApi;
    },
    columnDefs: [
      { field: 'date', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.date | date:"yyyy-MM-dd" }}</div>' },
      { field: 'branch' },
      { field: 'amount' },
      {
        field: 'amount', displayName: 'Actions', width: '10%', cellTemplate: '<div>\
          <button class="btn btn-primary btn-round btn-fab btn-fab-mini btn-info" ng-click="grid.appScope.openCashRecords(row.entity._id)">\
            <i class="material-icons">attach_money</i>\
          </button>\
        </div> '
      }
    ]
  }
  
  $http.get(url + '/receivedCash').then(function (response) {

    $rootScope.receivedCash = response.data.receivedCash.map(function (item) {
      item.date = new Date(item.date)
      return item
    })

    $scope.gridOptions.data = $rootScope.receivedCash

  }).catch(function (err) {
    console.log(err);
  })

  $scope.openCashRecords = function (_id) {
    window.open(url + '/dashboard/#/receivedCashDetails/' + _id, '_blank')
  }

})
