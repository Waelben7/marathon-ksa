app.controller('driverDocumentController', function ($scope, $rootScope, $http, $routeParams, ngDialog, Upload) {

  var url = window.location.origin
  $scope.today = new Date()

  $http.get(url + '/driver/' + $routeParams.driverId).then(function (response) {
    $rootScope.driver = response.data.driver

    if ($rootScope.driver.branch == 'Alhassa')
      $rootScope.driver.branch = 'الاحساء'
    else if ($rootScope.driver.branch == 'Eastern Region')
      $rootScope.driver.branch = 'المنطقة الشرقية'

  }).catch(function (err) {
    console.log(err);
  })

  $scope.print = function (type) {
    var myWindow = window.open('', '', 'width=794');
    myWindow.document.write(document.getElementById(type).innerHTML);

    myWindow.document.close();
    myWindow.focus();
    myWindow.print();
    myWindow.close();
  }
})